<?php
	// Fusiontables Details
	$tableid = 3185297;
	$username = "crushquiz";
	$password = "heriotwatt";
	//include the files from the PHP FT client library
	require('lib/fusion-tables-client-php/clientlogin.php');
	require('lib/fusion-tables-client-php/sql.php');
	// Log into fusiontables, get client object
	$ftclient = new FTClientLogin(ClientLogin::getAuthToken($username, $password));
	
	function getQuestionCount() {
		global $ftclient, $tableid;
		// Returns number of highest QUI in fusiontables
		return substr(trim($ftclient->query("SELECT QID FROM $tableid ORDER BY QID DESC LIMIT 1")),3);
	}
	
	function getCurrentQuestion() {
		if(@simplexml_load_file("data/currentQuestion.xml")==FALSE) {
			setCurrentQuestion(1);
		}
		$questionArray = (array) simplexml_load_file("data/currentQuestion.xml");
		return $questionArray;
	}
	
	function setRandQuestion() {
		// Advances to a new random question, out of all available questions in database (regardless of category etc)
		setCurrentQuestion(rand(0,getQuestionCount()));
	}
	
	function setCurrentQuestion($qid) {
		if(file_exists("data/setQlock")) return;
		touch("data/setQlock");
		// Sanitize requested question ID
		if($qid>=getQuestionCount()) $qid=1;
		//echo "SET CURRENT QUESTION: $qid<br /><br />";
		global $ftclient, $tableid;
		// Get specific QID question data 
		$selectquery = SQLBuilder::select($tableid,null, "QID = '$qid'");
		//echo $selectquery;
		$table_data = $ftclient->selectQueryMultiAssoc($selectquery);
		//echo "<textarea cols=80 rows=20>".print_r($table_data,1)."</textarea>";
		// Get contents of first row of data since we only requested one row from the fusiontable anyway
		$questionData = $table_data[0];
		// Add start time to question data to enable checking expiry time
		$questionData["startTime"]=time();
		$questionData["endTime"]=time()+$questionData["time"];
		// Form XML to store 
		$questionXML = new SimpleXMLElement('<currentQuestion/>');
		// Function call to convert array to xml
		array_to_xml($questionData,$questionXML);
		// Pretty-print XML output
		$questionDOM = dom_import_simplexml($questionXML)->ownerDocument;
		$questionDOM->formatOutput = true;
		// Move old currentQuestion
		unlink("/home/macsquiz/public_html/data/previousQuestion.xml");
		rename("/home/macsquiz/public_html/data/currentQuestion.xml","/home/macsquiz/public_html/data/previousQuestion.xml");
		
		// Clear out old responses for this question
		$responsesArray = (array) simplexml_load_file("data/responses.xml");
		foreach($responsesArray as $QID=>$QIDusers) {
			$responsesArray[$QID]=(array)$QIDusers;
			foreach($QIDusers as $username=>$QIDuserArray) {
				$responsesArray[$QID][$username]=(array)$QIDuserArray;
			}
		}
		if(isset($responsesArray["QID$qid"])) {
			unset($responsesArray["QID$qid"]);
		}
		$responsesXML = new SimpleXMLElement('<responses/>');
		array_to_xml($responsesArray,$responsesXML);
		$responsesDOM = dom_import_simplexml($responsesXML)->ownerDocument;
		$responsesDOM->formatOutput = true;
		file_put_contents("data/responses.xml",$responsesDOM->saveXML());
		// Write formatted XML to file
		file_put_contents("data/currentQuestion.xml",$questionDOM->saveXML());
		unlink("data/setQlock");
	}
	
	function array_to_xml($data, &$xml_data) {
		foreach($data as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml_data->addChild("$key");
					array_to_xml($value, $subnode);
				} else{
					array_to_xml($value, $xml_data);
				}
			} else {
				$xml_data->addChild("$key","$value");
			}
		}
	}
	
	if($_POST['action']=='currentQuestion') {
		$q = getCurrentQuestion(); 
		if($q['endTime']<=time()) {
			setCurrentQuestion($q['QID']+1);
			$q = getCurrentQuestion(); 
		}
		$timeleft=$q['endTime']-time();	
		
		echo <<<EOF
			<div class="well">
				<h2>Q{$q["QID"]}: {$q['question']}</h2>
				<p class="size20">Time Remaining: {$timeleft}</p>
			</div>
			<div class="well">
				<h2 class="btn btn-info h2button">A</h2> <h2 class="question">{$q['answerA']}</h2> <br />
				<h2 class="btn btn-success h2button">B</h2> <h2 class="question">{$q['answerB']}</h2> <br />
				<h2 class="btn btn-warning h2button">C</h2> <h2 class="question">{$q['answerC']}</h2> <br />
				<h2 class="btn btn-danger h2button">D</h2> <h2 class="question">{$q['answerD']}</h2> <br />
			</div>		
EOF;
		return;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>MACS Quiz</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="/css/custom.css" rel="stylesheet">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container-fluid"> 
			<div class="row-fluid">
				<div class="span3">
					<!--Sidebar content-->
					<div class="well pagination-centered">
						<img src="/img/macsquiz.png" alt="logo" /><br />
						<br />
						<a href='http://m.macsquiz.co.uk' onClick='window.open($(this).attr("href"), "MACS Quiz Mobile", "width=320,height=500,scrollbars=no"); return false;'>
							<img src='https://chart.googleapis.com/chart?chs=300x300&amp;cht=qr&amp;chl=http://m.macsquiz.co.uk&amp;chld=Q|0' alt='QR code'/>
						</a>
						<h1>Scan QR to play!</h1>
					</div>
				</div>
				<div class="span9">
					<!--Body content-->
					<div class="well">
						<h1 class="bottom20">Welcome to the non-stop quiz for MACS students and staff.</h1>
						<p class="size20">To play, scan the QR code on the left with your mobile phone.</p>
						<p class="size20">The current question is always displayed below. Good luck!</p>
					</div>
					<div id='currentQuestion'></div>
				</div>
			</div>			
		</div>
		<script type='text/javascript'>
			$(function() {
				$.post('questions.php', { action: 'currentQuestion' }, function(data) {
					$('#currentQuestion').html(data);						
				});
				var refreshId = setInterval(function() {
					$.post('questions.php', { action: 'currentQuestion' }, function(data) {
						$('#currentQuestion').html(data);					
					});
				}, 1000);
			});
		</script>
	</body>
</html>