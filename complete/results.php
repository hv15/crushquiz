<?php	
	if($_POST["action"]=="previousQuestionResults") {
		$PQ = (array) simplexml_load_file("data/previousQuestion.xml");

		$responsesArray = (array) simplexml_load_file("data/responses.xml");
		foreach($responsesArray as $QID=>$QIDusers) {
			$responsesArray[$QID]=(array)$QIDusers;
			foreach($QIDusers as $username=>$QIDuserArray) {
				$responsesArray[$QID][$username]=(array)$QIDuserArray;
			}
		}
		$PQID = "QID".$PQ["QID"];
		$PQresponseA=$PQresponseB=$PQresponseC=$PQresponseD=0;
		if(isset($responsesArray[$PQID])) {
			foreach($responsesArray[$PQID] as $username=>$userarray) {
				if($userarray["response"]=="A") {$PQresponseA++;$players.="<h3 class='btn btn-info h3button'>$username</h3> ";}
				if($userarray["response"]=="B") {$PQresponseB++;$players.="<h3 class='btn btn-success h3button'>$username</h3> ";}
				if($userarray["response"]=="C") {$PQresponseC++;$players.="<h3 class='btn btn-warning h3button'>$username</h3> ";}
				if($userarray["response"]=="D") {$PQresponseD++;$players.="<h3 class='btn btn-danger h3button'>$username</h3> ";}
			}
			$chartimg = '<img src="http://chart.apis.google.com/chart?chs=650x450&
						cht=p&
						chf=bg,s,00000000&
						chco=49afcd|5bb75b|faa732|da4f49&
						chxt=x& 
						chl=A|B|C|D&
						chxs=0,00000000,5.5&
						chd=t:'.$PQresponseA.','.$PQresponseB.','.$PQresponseC.','.$PQresponseD.'" 
				width="900" alt="'.$PQ["question"].'" />';
		} else {
			$chartimg = '<img src="/img/yuno.png" width="900" alt="'.$PQ["question"].'" />';
		}
		
		if($PQ["answer"]=="A") $answerletter = '<h3 class="btn btn-info h3button">A</h3>';
		if($PQ["answer"]=="B") $answerletter = '<h3 class="btn btn-success h3button">B</h3>';
		if($PQ["answer"]=="C") $answerletter = '<h3 class="btn btn-warning h3button">C</h3>';
		if($PQ["answer"]=="D") $answerletter = '<h3 class="btn btn-danger h3button">D</h3>';
		if(empty($players)) $players = "None!";
		
		echo <<<EOF
			<div class="well">
				<h2>Q{$PQ["QID"]}: {$PQ["question"]}</h2>
				<h3 class="inline">Answer: </h3> {$answerletter} <h3 class="inline">{$PQ["answer".$PQ["answer"]]}</h3>
			</div>
			<div class="well">
				<h2>Results:</h2>
				<div class="center bottom20">
					<h3 class="btn btn-info h3button">A</h3> <h3 class="inline"> => {$PQresponseA} </h3>
					<h3 class="btn btn-success h3button">B</h3> <h3 class="inline"> => {$PQresponseB} </h3>
					<h3 class="btn btn-warning h3button">C</h3> <h3 class="inline"> => {$PQresponseC} </h3>
					<h3 class="btn btn-danger h3button">D</h3> <h3 class="inline"> => {$PQresponseD} </h3>
				</div>
				{$chartimg}
				<h2>Players:</h2>
				{$players}
			</div>
EOF;
		return;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>MACS Quiz</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="/css/custom.css" rel="stylesheet">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container-fluid"> 
			<div class="row-fluid">
				<!--Sidebar content
				<div class="span2">
					<div class="well pagination-centered">
						<img src="/img/macsquiz.png" alt="logo" /><br />
						<br />
					</div>
				</div>
				-->
				<div class="span12">
					<!--Body content-->
					<div class="well">
						<h1 class="bottom20">MACS Quiz Results</h1>
						<p class="size20">The results for the previous question are shown below.</p>
					</div>
					<div id='previousQuestionResults'></div>
				</div>
			</div>			
		</div>
		<script type='text/javascript'>
			$(function() {
				$.post('results.php', { action: 'previousQuestionResults' }, function(data) {
					$('#previousQuestionResults').html(data);						
				});
				var refreshId = setInterval(function() {
					$.post('results.php', { action: 'previousQuestionResults' }, function(data) {
						$('#previousQuestionResults').html(data);						
					});
				}, 2000);
			});
		</script>
	</body>
</html>