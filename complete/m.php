<?php
	if($_GET['response']) {
		if(!isset($_COOKIE['cq_user'])){
			// User cookie expired, tell ajax loader to reload page
			echo 0;
			return;
		}
		$cookieuser = $_COOKIE['cq_user'];
		setcookie('cq_user',$cookieuser,time()+120); //add 2 minutes
		$response = $_GET['response'];

		$q = (array) simplexml_load_file("data/currentQuestion.xml");
		$qid = "QID".$q["QID"];
		
		if(@simplexml_load_file("data/responses.xml")==FALSE) {
			// XML File couild not be loaded, regenerate it
			$responsesXML = new SimpleXMLElement('<responses/>');
			$responsesArray = Array(
				$qid => Array(
					$cookieuser => Array(
						"response" => $response
					)
				)
			);
			array_to_xml($responsesArray,$responsesXML);
			$responsesDOM = dom_import_simplexml($responsesXML)->ownerDocument;
			$responsesDOM->formatOutput = true;
			file_put_contents("data/responses.xml",$responsesDOM->saveXML());
		} else {
			$responsesArray = (array) simplexml_load_file("data/responses.xml");
			foreach($responsesArray as $QID=>$QIDusers) {
				$responsesArray[$QID]=(array)$QIDusers;
				foreach($QIDusers as $username=>$QIDuserArray) {
					$responsesArray[$QID][$username]=(array)$QIDuserArray;
				}
			}
			if(isset($responsesArray[$qid][$cookieuser])) {
				// User already voted for this question, tell ajax loader to grey out buttons and alert user
				echo 1;
				return;
			} else {
				$responsesArray[$qid][$cookieuser] = Array(
					"response" => $response
				);
			}
			$responsesXML = new SimpleXMLElement('<responses/>');
			array_to_xml($responsesArray,$responsesXML);
			$responsesDOM = dom_import_simplexml($responsesXML)->ownerDocument;
			$responsesDOM->formatOutput = true;
			file_put_contents("data/responses.xml",$responsesDOM->saveXML());
			// Successful response, send the letter they clicked back to the ajax handler
			echo $response;
			return;
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>MACS Quiz</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="/css/bootstrap.min.css" rel="stylesheet">
		<link href="/css/bootstrap-responsive.min.css" rel="stylesheet">
		<link href="/css/custom.css" rel="stylesheet">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	</head>
	<body>
		<div class="container-fluid"> 
<?php
	// Check if cookie is set
	if(isset($_COOKIE['cq_user'])){ 
	
	// Fusiontables Details
	$tableid = 3185297;
	$username = "crushquiz";
	$password = "heriotwatt";
	//include the files from the PHP FT client library
	require('lib/fusion-tables-client-php/clientlogin.php');
	require('lib/fusion-tables-client-php/sql.php');
	// Log into fusiontables, get client object
	$ftclient = new FTClientLogin(ClientLogin::getAuthToken($username, $password));
	
	// If the user has a username (a COOKIE!!!), we update the users XML file
	$cookieuser = $_COOKIE['cq_user'];
	if(@simplexml_load_file("data/users.xml")==FALSE) {
		// XML File couild not be loaded, regenerate it
		$usersXML = new SimpleXMLElement('<users/>');
		$usersArray = Array(
			$cookieuser => Array(
				"expires" => time()+120
			)
		);
		array_to_xml($usersArray,$usersXML);
		$usersDOM = dom_import_simplexml($usersXML)->ownerDocument;
		$usersDOM->formatOutput = true;
		file_put_contents("data/users.xml",$usersDOM->saveXML());	
	} else {
		$usersArray = (array) simplexml_load_file("data/users.xml");
		foreach($usersArray as $key=>$val) {
			$usersArray[$key]=(array)$val;
			if($val["expires"]<time()) unset($usersArray[$key]);
		}
		if(isset($usersArray[$cookieuser])) {
			$usersArray[$cookieuser] = (array) $usersArray[$cookieuser];	
			$usersArray[$cookieuser]["expires"]=time()+120;
		} else {
			$usersArray[$cookieuser]=Array(
				"expires" => time()+120
			);
		}
		$usersXML = new SimpleXMLElement('<users/>');
		array_to_xml($usersArray,$usersXML);
		$usersDOM = dom_import_simplexml($usersXML)->ownerDocument;
		$usersDOM->formatOutput = true;
		file_put_contents("data/users.xml",$usersDOM->saveXML());
	}
	
	$q = (array) simplexml_load_file("data/currentQuestion.xml");
	$timeleft=$q['endTime']-time();
?>
			<div class="row-fluid">
				<div class="span12">
					<div class="well" id="questioninfo">
						<h2>Question <?php echo $q["QID"] ?></h2>
						<p class="size20">Time Remaining: <span id="countdown"></span>s</p>
						<p><small>Q: <?php echo $q['question'] ?></small></p>
					</div>
					<div class="well" id="answerbuttons">
						<p><small>A: <?php echo $q['answerA'] ?></small></p>
						<h2 class="btn btn-info h2button displayblock A">A</h2>
						<p><small>B: <?php echo $q['answerB'] ?></small></p>
						<h2 class="btn btn-success h2button displayblock B">B</h2>
						<p><small>C: <?php echo $q['answerC'] ?></small></p>
						<h2 class="btn btn-warning h2button displayblock C">C</h2>
						<p><small>D: <?php echo $q['answerD'] ?></small></p>
						<h2 class="btn btn-danger h2button displayblock D">D</h2>
					</div>
				</div>
			</div>
			<script type="text/javascript">
				$(function() {
					var seconds = <?php echo $timeleft ?>;
					setInterval(updateCountdown, 1000);
					
					$(".h2button").click(function(){
						$.get('?response='+$(this).text(), function(data) {
							if(data==0) {
								window.location=window.location;
							} else if(data==1) {
								$("#questioninfo").html('<h2>You have already answered this question!</h2><p class="size20">Time till next question: <span id="countdown"></span>s</p>');
								$(".h2button").css({ 'cursor' : 'default', 'opacity' : 0.3 }).off('mouseenter mouseleave click');
							} else {
								$("#questioninfo").html('<h2>Answer submitted!</h2><p class="size20">Next question in: <span id="countdown"></span>s</p>');
								$("."+data).siblings().css({ 'opacity' : 0.3 });
								$(".h2button").css({ 'cursor' : 'default' }).off('mouseenter mouseleave click');
							}
						});
					});
					
					function updateCountdown() {
					  if (seconds > 0) {
						seconds--;
						$("#countdown").text(seconds);
					  } else {
						document.location.reload(true);
					  }
					}
				});
			</script>
<?php } else { ?>
			<div class="row-fluid">
				<div class="span12">
					<form class="well form-vertical" method="get" action="?" onSubmit="setCookie(username.value)">
						<h4 class="bottom10">Enter your username:</h4>
						<input type="text" class="input-small" name="username" placeholder="Username" /><span class="help-inline">@hw.ac.uk</span><br />
						<button type="submit" class="btn">Submit</button>
					</form>
				</div>
			</div>
			<script type="text/javascript">
				function setCookie(username){
					var now = new Date();
					now.setTime(now.getTime() + (2*60*1000)); // 2 minutes
					document.cookie = "cq_user="+username+"; expires="+now.toUTCString()+"; path=/";
					location.reload(true);
				}
			</script>
<?php } 
	function array_to_xml($data, &$xml_data) {
		foreach($data as $key => $value) {
			if(is_array($value)) {
				if(!is_numeric($key)){
					$subnode = $xml_data->addChild("$key");
					array_to_xml($value, $subnode);
				} else{
					array_to_xml($value, $xml_data);
				}
			} else {
				$xml_data->addChild("$key","$value");
			}
		}

	}
?>
		</div>
	</body>
</html>