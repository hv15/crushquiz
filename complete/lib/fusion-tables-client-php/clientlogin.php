
<?php

include_once "constants.php";

class ClientLogin {
  public static function getAuthToken($username, $password) {
	$clientlogin_curl = curl_init();
	curl_setopt($clientlogin_curl,CURLOPT_URL,'https://www.google.com/accounts/ClientLogin');
	curl_setopt($clientlogin_curl, CURLOPT_POST, true); 
	curl_setopt ($clientlogin_curl, CURLOPT_POSTFIELDS,
		"Email=".$username."&Passwd=".$password."&service=fusiontables&accountType=GOOGLE");
	curl_setopt($clientlogin_curl,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($clientlogin_curl,CURLOPT_RETURNTRANSFER,1);
	$token = curl_exec($clientlogin_curl);
	curl_close($clientlogin_curl);
	$token_array = explode("=", $token);
	$token = str_replace("\n", "", $token_array[3]);
	return $token;
  }
}


class FTClientLogin {
  function __construct($token) {
	$this->token = $token;
  }
  
  function query($query, $gsessionid = null, $recursedOnce = false) {
	
	$url = URL;
   	$query =  "sql=".urlencode($query);
   	
	$fusiontables_curl=curl_init();
	if(preg_match("/^select|^show tables|^describe/i", $query)) { 
   	  $url .= "?".$query;
	  if($gsessionid) { $url .= "&gsessionid=$gsessionid"; }
	  curl_setopt($fusiontables_curl,CURLOPT_HTTPHEADER, array("Authorization: GoogleLogin auth=".$this->token));
	
	} else {
	  if($gsessionid) { $url .= "?gsessionid=$gsessionid"; }
	  
	  //set header
	  curl_setopt($fusiontables_curl,CURLOPT_HTTPHEADER, array( 
		"Content-length: " . strlen($query), 
		"Content-type: application/x-www-form-urlencoded", 
		"Authorization: GoogleLogin auth=".$this->token		 
	  )); 
	  
	  //set post = true and add query to postfield
	  curl_setopt($fusiontables_curl,CURLOPT_POST, true);
	  curl_setopt($fusiontables_curl,CURLOPT_POSTFIELDS,$query); 
	}
	
	curl_setopt($fusiontables_curl,CURLOPT_URL,$url);
	curl_setopt($fusiontables_curl,CURLOPT_CONNECTTIMEOUT,2);
	curl_setopt($fusiontables_curl,CURLOPT_RETURNTRANSFER,1);
	$result = curl_exec($fusiontables_curl);
	curl_close($fusiontables_curl);
	
	//If the result contains moved Temporarily, retry
	if (strpos($result, '302 Moved Temporarily') !== false) {
	  preg_match("/(gsessionid=)([\w|-]+)/", $result, $matches);
	  
	  if (!$matches[2]) { return false; }

	  if ($recursedOnce === false) {
		return $this->query($url, $matches[2], true);
	  }
	  return false;
	}

	return $result;
  }
  
	function selectQueryMultiAssoc($query, $gsessionid = null, $recursedOnce = false) {
		$result = trim($this->query($query, $gsessionid, $recursedOnce));
		// echo "<textarea cols=80 rows=20>".print_r($result,1)."</textarea><br />";
		$rows = explode("\n", $result);
		$cols = explode(',',$rows[0]);
		array_splice($rows,0,1);
		foreach($rows as $rownum=>$rowcsv) {
			// Check for commas in data and escape them so explode doesn't get confused
			while(preg_match('|"([^,"]+),(.+?)"|',$rowcsv)) {
				$rowcsv=preg_replace('|"([^,]+),(.+?)"|','"$1&#44;$2"',$rowcsv);
				//if(++$whilecheck>50) die("ran while loop $whilecheck times");
			}
			//echo "all commas gone!<br />";
			//echo "<textarea cols=80 rows=20>".print_r($rowcsv,1)."</textarea>";
			$rowcsv=preg_replace('|"(.+?)"|','$1',$rowcsv);
			//echo "<textarea cols=80 rows=20>".print_r($rowcsv,1)."</textarea>";
			$rowdata=explode(',',$rowcsv);
			foreach($rowdata as $datafield=>$data) {
				$rowdataAssoc[$cols[$datafield]]=$data;
			}
			$output[$rownum]=$rowdataAssoc;
		}
		return $output;
	}
}

?>
