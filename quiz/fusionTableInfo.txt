tableID: 3185297

Columns
question : text
answers : text (pipe delimited)
answer : int (zero based position in answers)
time : int (seconds)
questionType : int (0 : multiple choice)
category : int (0 : geography, 1 : common sense)
